{
  description =
    "An flake to use a Python poetry project in an FHS environment when poetry2nix is uncooperative";
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        fhsEnv = (pkgs.buildFHSUserEnv rec {
          name = "mypackage-env";
          targetPkgs = pkgs:
            (with pkgs; [
              python3
              poetry
              # add the system package here that are needed for the Python package dependencies
              libz # needed for 'numpy'
            ]);
          profile = ''
            # Why is this necessary from the nix flake?
            export LD_LIBRARY_PATH="/lib:$LD_LIBRARY_PATH"
            # create and fill virtual environment
            poetry install --no-root # add --no-root here if this is just a metapackage
            # enter virtual environment
            source "$(poetry env info --path)"/bin/activate
          '';
        }).env;
      in { devShells.default = fhsEnv; });
}

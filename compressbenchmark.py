#!/usr/bin/python3
# system modules
import argparse
import csv
import datetime
import glob
import hashlib
import itertools
import os
import re
import shlex
import shutil
import subprocess
import tempfile
import textwrap
import time
import socket
import collections

# external modules
import attrs
import psutil
import rich
from rich.console import Console
from rich.logging import RichHandler
from rich.progress import (
    BarColumn,
    Progress,
    TaskProgressColumn,
    TextColumn,
    TimeRemainingColumn,
)

# internal modules
import yanns_python_utils

console = Console()


@attrs.frozen
class CompressionAlgorithm:
    executable: str = attrs.field()
    level: int = attrs.field()

    @property
    def version(self):
        output = subprocess.check_output([self.executable, "--version"]).decode(
            errors="ignore"
        )
        return next(iter(re.findall(r"\D(\d+\.\d+\.\d+)", output)), "?")

    def cmdline_compression(self) -> str:
        return f"{self.executable} -{self.level} --force"

    def cmdline_decompression(self) -> str:
        return f"{self.executable} -d --force"

    def benchmark(self, direction, files, progress=None, **popen_kwargs):
        cmdline = {
            "compression": self.cmdline_compression(),
            "decompression": self.cmdline_decompression(),
        }[direction]
        rss_max_field = f"ram_rss_max_{direction}"
        result = {rss_max_field: 0, f"cmdline_{direction}": cmdline}
        if progress:
            task = progress.add_task(f"{cmdline!r}", total=None)
        time_before = time.perf_counter()
        process = subprocess.Popen(
            shlex.split(cmdline) + files,
            **{
                **dict(
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                    stdin=subprocess.DEVNULL,
                ),
                **popen_kwargs,
            },
        )
        console.log(f"🚀 Started process {process.pid} {cmdline!r}")
        proc = psutil.Process(process.pid)
        while process.poll() is None:
            rss = sum(
                p.memory_info().rss for p in [proc] + proc.children(recursive=True)
            )
            result[rss_max_field] = max(result[rss_max_field], rss)
            if progress:
                progress.update(
                    task,
                    description=f"{cmdline!r}: "
                    f"(⏱️ {time.perf_counter() - time_before:6.1f}s) "
                    f"RAM (RSS): {result[rss_max_field]}",
                )
        process.wait()
        result[f"time_{direction}"] = time.perf_counter() - time_before
        console.log(
            f"{'⚠️ ' if process.returncode else '✅'} "
            f"Process {process.pid} {cmdline!r} "
            f"finished with return code {process.returncode}"
        )
        if progress:
            progress.remove_task(task)
        return result


@attrs.frozen
class XZ(CompressionAlgorithm):
    executable: str = attrs.field(default="xz")
    level: int = attrs.field(validator=attrs.validators.in_(range(0, 10)), default=0)


@attrs.frozen
class GZIP(CompressionAlgorithm):
    executable: str = attrs.field(default="gzip")
    level: int = attrs.field(validator=attrs.validators.in_(range(1, 10)), default=1)


@attrs.frozen
class ZSTD(CompressionAlgorithm):
    executable: str = attrs.field(default="zstd")
    level: int = attrs.field(
        validator=attrs.validators.in_(range(1, 22 + 1)), default=1
    )
    threads: int = attrs.field(
        validator=attrs.validators.in_(range(1, os.cpu_count() + 1)), default=1
    )
    fast: bool = attrs.field(default=False)

    def cmdline_compression(self) -> str:
        return f"""{self.executable} -T{self.threads} {"--ultra " if self.level >= 20 else ""}{"--fast=" if self.fast else "-"}{self.level} --force --rm"""

    def cmdline_decompression(self) -> str:
        return f"""{self.executable} -T{self.threads} -d --force --rm"""


@attrs.frozen
class LZ4(CompressionAlgorithm):
    executable: str = attrs.field(default="lz4")
    level: int = attrs.field(validator=attrs.validators.in_(range(1, 13)), default=1)
    fast: bool = attrs.field(default=False)

    def cmdline_compression(self) -> str:
        return f"""{self.executable} {"--fast=" if self.fast else "-"}{self.level} --force --rm"""

    def cmdline_decompression(self) -> str:
        return f"""{self.executable} -d --force --rm"""


def default_output_filename(note=""):
    try:
        lscpu = subprocess.check_output(["lscpu"], env=dict(LC_ALL="C")).decode(
            errors="ignore"
        )
        processor = yanns_python_utils.normalize_filename(
            re.findall(r"Model\s*name:\s*(.*)", lscpu, flags=re.IGNORECASE)[0],
        )
    except (FileNotFoundError, subprocess.CalledProcessError, IndexError):
        processor = "unknown-CPU"

    return (
        datetime.datetime.now()
        .strftime(
            "%Y-%m-%dT%H.%M.%S.%f%z-"
            "compressbenchmark-"
            "{hostname}-{uname.machine}-{processor}{note}.csv"
        )
        .format(
            hostname=socket.gethostname(),
            uname=os.uname(),
            processor=processor,
            note=yanns_python_utils.normalize_filename(f"-{note}" if note else ""),
        )
    )


parser = argparse.ArgumentParser(description="🗜️ Compression Algorithm Benchmark ⏱️")
parser.add_argument("--tmpdir", help="Temporary base directory to usee")
parser.add_argument("-o", "--output", help="Output CSV file.")
parser.add_argument(
    "--note", help="Extra string to be put into filename and 'note' column", type=str
)
parser.add_argument(
    "--exclude",
    nargs="*",
    help="pattern(s) to exclude cmdlines to test",
    type=lambda s: re.compile(s, flags=re.IGNORECASE),
    default=list(),
)
parser.add_argument(
    "--only",
    nargs="*",
    help="pattern(s) to filter cmdlines to test",
    type=lambda s: re.compile(s, flags=re.IGNORECASE),
    default=list(),
)
parser.add_argument(
    "path",
    nargs="+",
    help="(folders of) files to benchmark the compression with",
)


def get_field_options(field):
    if options := getattr(getattr(field, "validator", None), "options", None):
        return options
    elif field.type is bool:
        return (True, False)
    return None


def algorithm_combinations():
    for algocls in CompressionAlgorithm.__subclasses__():
        for kwargs in yanns_python_utils.all_argument_combinations(
            {
                # meh, get_field_options() called twice 🫤
                f.name: get_field_options(f)
                for f in attrs.fields(algocls)
                if get_field_options(f)
            }
        ):
            try:
                yield algocls(**kwargs)
            except TypeError as e:
                pass


def b2sum(path):
    h = hashlib.new("sha1")
    with open(path, "rb") as fh:
        h.update(fh.read(2**16))
    return h.hexdigest()


def cli():
    args = parser.parse_args()

    output = open(
        args.output if args.output else default_output_filename(note=args.note), "w"
    )

    console.log(f"Output file is {output.name!r}")

    csvwriter = csv.DictWriter(
        output,
        fieldnames=sorted(
            set(
                itertools.chain.from_iterable(
                    [
                        (f.name for f in attrs.fields(a))
                        for a in CompressionAlgorithm.__subclasses__()
                    ]
                )
            )
        )
        + [
            "version",
            "cmdline_compression",
            "cmdline_decompression",
            "note",
            "n_files",
            "file_suffixes",
            "size_uncompressed",
            "size_compressed",
            "time_compression",
            "time_decompression",
            "ram_rss_max_compression",
            "ram_rss_max_decompression",
        ],
    )

    csvwriter.writeheader()

    with tempfile.TemporaryDirectory(
        prefix=f"compressbenchmark-",
        ignore_cleanup_errors=True,
        dir=args.tmpdir,
    ) as tmpdir:
        console.log(f"{tmpdir = }")
        for path in args.path:
            if os.path.isdir(path):
                console.log(f"Copying dir {path!r} to {tmpdir!r}...")
                shutil.copytree(path, tmpdir, dirs_exist_ok=True)
            else:
                console.log(f"Copying {path!r} to {tmpdir!r}...")
                shutil.copy(path, tmpdir)

        def files_in_tempdir():
            return [
                p for p in glob.glob(os.path.join(tmpdir, "**")) if os.path.isfile(p)
            ]

        # check files
        checksums = {p: b2sum(p) for p in files_in_tempdir()}

        def assert_checksums():
            for p, c in checksums.items():
                assert c == b2sum(p), f"{p = }, {b2sum(p) = } != {c}"
            console.log("✅ Checksums OK")

        def assert_only_original_files_exist():
            assert set(files_in_tempdir()) == set(
                checksums
            ), f"Only original files should exist, but:\n{files_in_tempdir() = },\n{checksums = }"

        def assert_original_files_dont_exist():
            existing_original_files = set(files_in_tempdir()).intersection(
                set(checksums)
            )
            assert (
                not existing_original_files
            ), f"Original files shouldn't exist, but {existing_original_files = }"

        def count_size():
            return sum(os.stat(p).st_size for p in files_in_tempdir())

        console.log(f"{checksums = }")
        with Progress(
            TimeRemainingColumn(),
            TaskProgressColumn(),
            BarColumn(),
            TextColumn("[progress.description]{task.description}"),
            transient=True,
            console=console,
        ) as progress:
            combinations = list(algorithm_combinations())
            task = progress.add_task(description="total", total=len(combinations))
            for i, alg in enumerate(combinations, start=1):
                if any(p.search(alg.cmdline_compression()) for p in args.exclude):
                    console.log(
                        f"⏩ Skipping {alg.cmdline_compression()!r} due to --exclude"
                    )
                    progress.update(task, advance=1)
                    continue
                if args.only and not any(
                    p.search(alg.cmdline_compression()) for p in args.only
                ):
                    console.log(
                        f"⏩ Skipping {alg.cmdline_compression()!r} for not being in --only"
                    )
                    progress.update(task, advance=1)
                    continue
                console.log(f"{i = }  {alg = }")
                row = {"version": alg.version}
                if args.note:
                    row["note"] = args.note
                row.update(attrs.asdict(alg))
                assert_only_original_files_exist()
                assert_checksums()
                row["n_files"] = len(files_in_tempdir())
                row["file_suffixes"] = " ".join(
                    f"{v}*{k}"
                    for k, v in sorted(
                        collections.Counter(
                            itertools.chain.from_iterable(
                                re.findall(r"(?:\.[a-z0-9]+)+$", f, flags=re.IGNORECASE)
                                for f in files_in_tempdir()
                            )
                        ).items()
                    )
                )
                row["size_uncompressed"] = count_size()
                # compression
                benchmark_kwargs = dict(stdout=None) if "lz4" in alg.executable else {}
                result = alg.benchmark(
                    "compression", files_in_tempdir(), progress, **benchmark_kwargs
                )
                console.log(f"{result = }")
                row.update(result)
                assert_original_files_dont_exist()
                row["size_compressed"] = count_size()
                progress.update(task, advance=0.5)
                # decompression
                result = alg.benchmark(
                    "decompression", files_in_tempdir(), progress, **benchmark_kwargs
                )
                console.log(f"{result = }")
                row.update(result)
                assert_only_original_files_exist()
                assert_checksums()
                console.log(row)
                csvwriter.writerow(row)
                output.flush()
                progress.update(
                    task,
                    description=f"total ({i}/{len(combinations)} done)",
                    advance=0.5,
                )

    console.log(f"📥 Output written to {getattr(args.output,'name','somewhere!?')!r}")


if __name__ == "__main__":
    cli()

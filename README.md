# 🗜️ Compression Algorithm Benchmark

## Usage

```bash
# Clone repo with submodules
git clone --recursive https://gitlab.com/nobodyinperson/compression-algorithm-benchmark
# enter poetry environment
poetry install;poetry shell

# or just enter a dev env with nix
nix develop gitlab:nobodyinperson/compression-algorithm-benchmark
```

```bash
# Run the script in this repo with paths to files you want to benchmark (files will be copied to tmpfs)
compressbenchmark ~/Downloads/20200612151436-arduino-fixed.csv
```

Output file like `2022-11-25T13.44.59.079592-compressbenchmark.csv` will be created looking like this:

```csv
executable,fast,level,threads,size_uncompressed,size_compressed,time_compression,time_decompression,ram_rss_max_compression,ram_rss_max_decompression
xz,,0,,72369420,7238872,1.8979203120106831,0.6053836509818211,4837376,2146304
xz,,1,,72369420,7301236,3.317013039952144,0.6216167761012912,10801152,3051520
xz,,2,,72369420,7269060,6.5634723079856485,0.7509379559196532,17367040,4067328
xz,,3,,72369420,7348700,12.778336968040094,0.6635462749982253,29200384,6332416
xz,,4,,72369420,7874860,9.60053302894812,0.5972813080297783,45903872,6332416
xz,,5,,72369420,6755636,23.358219380956143,0.6018311280058697,85864448,10387456
xz,,6,,72369420,4679708,52.37358859600499,0.5201061870902777,86126592,10235904
xz,,7,,72369420,4679240,52.40237103996333,0.510799283045344,165883904,18604032
xz,,8,,72369420,4683264,54.02455270499922,0.5280451020225883,325373952,35401728
...
```

## 📈 Plots

You can run the Juypter notebook in this repo to generate some plots.
